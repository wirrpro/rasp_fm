FROM python:3.6.3

ADD requirements.txt /app/requirements.txt

WORKDIR /app

# DEV
VOLUME /app

RUN pip install -r ./requirements.txt

RUN adduser --disabled-password --gecos '' rasper
