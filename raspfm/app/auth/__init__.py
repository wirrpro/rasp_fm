"""
Auth package:
- login, register (stored in app)
- OAuth with Last.fm

"""

from flask import Blueprint

auth = Blueprint('auth', __name__)

from . import errors, forms, views

