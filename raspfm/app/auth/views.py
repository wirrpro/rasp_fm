from flask import render_template as rt
from . import auth



@auth.route('/login')
def login():
    """App login page"""
    return rt('auth/login.html')


@auth.route('/register')
def register():
    """App signUp/register page"""
    return rt('auth/register.html')


@auth.route('/restore_password')
def resume():
    """Restore password page"""
    return rt('auth/restore_password.html')