"""
API:
    - POST:
        scrobbling
    - GET:
        scrobbled songs
        albums
        statistics
        etc

"""

from flask import Blueprint

api = Blueprint('api_1_0', __name__)

# from . import views
