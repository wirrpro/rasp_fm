from flask import render_template as rt
from . import main


@main.route('/')
def index():
    """Index page"""
    return rt('main/index.html')


@main.route('/albums')
def albums():
    """Index page"""
    return rt('main/albums.html')


@main.route('/bands')
def bands():
    """Index page"""
    return rt('main/bands.html')


@main.route('/history')
def history():
    """Index page"""
    return rt('main/history.html')


@main.route('/upload', methods=['POST'])
def upload():
    pass