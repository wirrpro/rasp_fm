"""
Core app package.
Show us information about scrobbles, listened etc
from db to views

"""

from flask import Blueprint

main = Blueprint('main', __name__)

from . import views, errors, forms