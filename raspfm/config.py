import os


basedir = os.path.abspath(os.path.dirname(__file__))



class Config:
    SECRET_KEY = 'secret'
    SQLALCHEMY_ECHO = True  # for debug
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # SQLALCHEMY_RECORD_QUERIES = True
    CELERY_BROKER_URL = 'amqp://celery:celerypassword@rabbit:5672/pptp'

    @staticmethod
    def init_app(app):
        pass


class DevelopConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URI') or \
                              'postgresql://squidward:Parolsquidward#@db/pptp'


class TestingConfig(Config):
    TESTING = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URI') or \
                              'postgresql://squidward:Parolsquidward#@db/pptp'


class ProductionConfig(Config):
    TESTING = False
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('PROD_DATABASE_URI') or \
                              'postgresql://squidward:Parolsquidward#@db/pptp'



config = {
    'development': DevelopConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,

    'default': DevelopConfig
}